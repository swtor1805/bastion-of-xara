﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class DestroyImpactEffect1 : MonoBehaviour
{
    public Light2D impacteffect;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "UI")
        {
            impacteffect.intensity = 0;
        }
        else if (collision.collider.tag == "Enemy")
        {
            impacteffect.intensity = 0;
        }
    }
}
