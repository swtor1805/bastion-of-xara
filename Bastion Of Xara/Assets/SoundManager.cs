﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip playerHit;
    public static AudioClip jumpSound;
    static AudioSource audioSrc;
    // Start is called before the first frame update
    void Start()
    {
        playerHit = Resources.Load<AudioClip>("Hit23");
        jumpSound = Resources.Load<AudioClip>("Jump13");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "playerhit":
                audioSrc.PlayOneShot(playerHit);
                    break;
            case "jump":
                audioSrc.PlayOneShot(jumpSound);
                break;

        }
    }
}
