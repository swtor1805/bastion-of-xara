﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateButton : MonoBehaviour
{
    public GameObject Button;
    // Start is called before the first frame update
    public void buttonActivation()
    {
        Button.SetActive(true);
    }
}
