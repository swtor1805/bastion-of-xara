﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelCompleted : MonoBehaviour
{
    public GameObject LevelFinished;
    GameObject health;
    public Text Title;
    public AudioSource Music;
    public AudioSource BulletSound;


    public void LevelDone()
    {
        Music.volume = 0;
        BulletSound.volume = 0;
        health = GameObject.Find("HealthBar");
        health.SetActive(false);
        LevelFinished.SetActive(true);
        Title.text = "Level " + (FindObjectOfType<Player>().playerIsInLevel - 1) + " Complet";
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(FindObjectOfType<Player>().playerIsInLevel + 1);
    }

    public void Restart()
    {
        SceneManager.LoadScene(FindObjectOfType<Player>().playerIsInLevel);
    }

}
