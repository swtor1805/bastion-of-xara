﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Login 
{
    private static readonly HttpClient client = new HttpClient();

    public static void LoginUser(User user)
    {

        var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:3000/api/user/login");
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";

        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
            Debug.Log(JsonUtility.ToJson(user));
            streamWriter.Write(JsonUtility.ToJson(user));
        }

        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
            User result = JsonUtility.FromJson<User>(streamReader.ReadToEnd());
            if (result.ownsGame)
            {
                MainMenu.userId = result.userId;
                SceneManager.LoadScene("MainMenu");
            }
            else
            {
                //ToDo
            }
            Debug.Log(result.ToString());
        }
    }
}
