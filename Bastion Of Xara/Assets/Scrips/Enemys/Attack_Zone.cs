﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack_Zone : MonoBehaviour
{
    public TurretAI turretAI;

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            turretAI.ChekcIfTimeToFire();
        }
    }
}
