﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackZone_EnemyPlatformPatrol : MonoBehaviour
{
    public EnemyGroundShotAI enemy;

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            enemy.ChekcIfTimeToFire();
        }
    }
}
