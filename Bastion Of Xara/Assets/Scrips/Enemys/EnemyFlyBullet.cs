﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlyBullet : MonoBehaviour
{
    float moveSpeed = 7f;
    public Rigidbody2D rb;
    public int damage = 25;
    Player target;
    Vector2 moveDirection;
    public GameObject impactEffect;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        target = GameObject.FindObjectOfType<Player>();
        moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
        rb.velocity = new Vector2(moveDirection.x, moveDirection.y);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Instantiate(impactEffect, transform.position, Quaternion.identity);
        FindObjectOfType<Player>().TakeDamage(damage);

    }

}
