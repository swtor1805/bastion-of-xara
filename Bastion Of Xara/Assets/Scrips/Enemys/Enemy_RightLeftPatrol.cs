﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_RightLeftPatrol : MonoBehaviour
{
    public float speed;

    public bool MoveRight;

    public GameObject test;

    void Update()
    {
        if (MoveRight)
        {
            transform.Translate(2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector2(-System.Math.Abs(transform.localScale.x), transform.localScale.y);
        }
        else
        {
            transform.Translate(-2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector2(System.Math.Abs(transform.localScale.x), transform.localScale.y);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("turn"))
        {
            if (MoveRight)
            {
                MoveRight = false;
            }
            else
            {
                MoveRight = true;
            }
        }
        else if (collision.gameObject.CompareTag("EnemyDeath"))
        {
            Destroy(test);
        }
    }
}
