﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBullet : MonoBehaviour
{
    public float speed = 20f;
    public int damage = 10;
    public Rigidbody2D rb;
    public GameObject impactEffect;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right  * speed;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(gameObject);
        if (col.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<Player>().TakeDamage(damage);
            Destroy(gameObject);

        }
        else if (col.gameObject.CompareTag("UI"))
        {
            Destroy(gameObject);
        }
    }
}
