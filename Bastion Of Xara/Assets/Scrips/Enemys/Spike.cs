﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    Rigidbody2D rb;
    int damage = 5;
    public GameObject uIGameObjekt;
    int second;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name.Equals("Player"))
        {
            rb.isKinematic = false;
        }
        if (col.gameObject.CompareTag("UI"))
        {
            second = 1;
            Waiter();
        }

    }



    IEnumerator Waiter()
    {
        yield return new WaitForSeconds(second);

        Destroy(uIGameObjekt);
    }
}
