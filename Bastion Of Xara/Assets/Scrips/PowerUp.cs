﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public GameObject pickUpEffect;
    public HealthBar healthBar;
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PickUp(collision);
        }
    }

    void PickUp(Collider2D col)
    {
        Instantiate(pickUpEffect, transform.position, transform.rotation);
        Player player = col.GetComponent<Player>();
        player.currentHealth = 100;
        healthBar.SetHealth(player.currentHealth);
        Destroy(gameObject);
    }
}
