﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TextTimer : MonoBehaviour
{
    //[Header("Text")]
    [SerializeField] private Text text;
    [SerializeField] private char characterSpliter = ':';

    public string PlayerTime;
    public int playedTime;

    private float timer;
    private bool isActive;

    public void Start()
    {
        StartTimer();
    }

    // Update is called once per frame
    public void Update()
    {
        if (isActive)
        {
            timer += Time.deltaTime;
            UpdateText();
        }
    }

    private void UpdateText()
    {
        float seconds = (timer % 60);
        float minutes = (int)(timer / 60 % 60);
        float hours = (int)(timer / 3600 % 60);


        text.text = hours.ToString("00") + characterSpliter + minutes.ToString("00") + characterSpliter + seconds.ToString("00.00");
        PlayerTime = hours.ToString("00") + characterSpliter + minutes.ToString("00") + characterSpliter + seconds.ToString("00.00");
        playedTime = Convert.ToInt32((hours * 3600) + (minutes * 60) + seconds);
    }

    public void StartTimer()
    {
        StartTimer(0);
    }

    public void StartTimer(float seconds)
    {
        isActive = true;
        timer = seconds;
        UpdateText();
    }
}
