﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;


public class PlayerCollision : MonoBehaviour
{
    public GameObject button;
    public PlayerMovment movment;
    public void OnCollisionEnter2D(Collision2D collisionInfo)
    {
        switch (collisionInfo.gameObject.tag)
        {
            case "Enemy":
            case "Bullet":
            case "Spike":
                FindObjectOfType<Player>().TakeDamage(10);
                FindObjectOfType<PlayerMovment>().PlayerHurting();
                Invoke("playerHurted", 0.2f);
                break;
        }
    }

    public void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            FindObjectOfType<Player>().TakeDamage(1);
            FindObjectOfType<PlayerMovment>().PlayerHurting();
            Invoke("playerHurted", 0.2f);
        }

    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Dialog":
                Cursor.visible = true;
                button.SetActive(true);
                break;

            case "LevelCompleted":
                FindObjectOfType<LevelComplete>().LevelDone();
                break;
        }
    }

    public void playerHurted()
    {
        FindObjectOfType<PlayerMovment>().PlayerWasHurt();
    }

}
