﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Player : MonoBehaviour
{

    public int maxHealth = 100;
    public int currentHealth;
    public int playerIsInLevel;
    public float restartDelay = 2f;
    public HealthBar healthBar;
    public GameObject GameOverMenu;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        playerIsInLevel = Convert.ToInt32(SceneManager.GetActiveScene().name);
    }

    public void TakeDamage(int damage)
    {
        if (FindObjectOfType<HealthBar>().slider.value == 0)
        {
            FindObjectOfType<GameOverScript>().GameOver();
        }
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
    }
}
