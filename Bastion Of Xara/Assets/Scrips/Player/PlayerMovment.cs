﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{
    public CharacterController2D controller;
    public float runspeed = 40f;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;
    bool shoot = false;
    public Rigidbody2D rb;
    public Animator animator;


    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runspeed;
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
        //Debug.Log(Input.GetAxisRaw("Horizontal")); <-- Get the Position for Debuging

        if (Input.GetButtonDown("Jump"))
        {
            SoundManager.PlaySound("jump");
            jump = true;
            //animator.SetBool("IsJumping", true);
        }

        if (Input.GetButtonDown("Crouch"))
        {
            crouch = true;
            animator.SetBool("IsCrouching", true);

        }
        else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
            animator.SetBool("IsCrouching", false);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            shoot = true;
            animator.SetBool("IsShooting", true);
        }
        else if(Input.GetButton("Fire1") && Input.GetKey("a") || Input.GetButton("Fire1") && Input.GetKey("d"))
        {
            shoot = false;
            animator.SetBool("IsShooting", false);
            shoot = true;
            animator.SetBool("IsRunningAndShooting", true);

        }
        else if (Input.GetButton("Fire1") && !Input.GetKey("a") || Input.GetButton("Fire1") && !Input.GetKey("d"))
        {
            shoot = false;
            animator.SetBool("IsRunningAndShooting", false);
            shoot = true;
            animator.SetBool("IsShooting", true);

        }
        else if (Input.GetButtonUp("Fire1"))
        {
            shoot = false;
            animator.SetBool("IsShooting", false);
        }

        if (Input.GetButtonDown("Fire1") && Input.GetKey("a") || Input.GetButtonDown("Fire1") && Input.GetKey("d"))
        {
            shoot = true;
            animator.SetBool("IsRunningAndShooting", true);
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            shoot = false;
            animator.SetBool("IsRunningAndShooting", false);
        }
    }

    public void PlayerHurting()
    {
        animator.SetBool("IsHurting", true);
        SoundManager.PlaySound("playerhit");
    }

    public void PlayerWasHurt()
    {
        animator.SetBool("IsHurting", false);
    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
    }

    void FixedUpdate()
    {
        //Move our Character
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }
}

