﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerContoller2D : MonoBehaviour
{
    Animator animator;
    Rigidbody2D rb2d;
    SpriteRenderer spriteRenderer;

    bool isGrounded;

    [SerializeField]
    Transform groundCheck;
    [SerializeField]
    private float runSpeed = 3f;
    [SerializeField]
    private float jumpForce = 5f;

    // Start is called before the first frame update
    void Start()
    {
        animator.GetComponent<Animator>();
        rb2d.GetComponent<Rigidbody2D>();
        spriteRenderer.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if(Physics2D.Linecast(transform.position, groundCheck.position, 5 << LayerMask.NameToLayer("UI")))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }

        if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            rb2d.velocity = new Vector2(runSpeed, rb2d.velocity.y);

            if(isGrounded)
                animator.Play("NewPlayer_Run");
        }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            rb2d.velocity = new Vector2(-runSpeed, rb2d.velocity.y);

            if (isGrounded)
                animator.Play("NewPlayer_Run");
        }
        else
        {
            if (isGrounded)
                animator.Play("NewPlayer_Idle");
        }

        if (Input.GetKey(KeyCode.Space))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
            animator.Play("NewPlayer_Jump");
        }
    }
}
