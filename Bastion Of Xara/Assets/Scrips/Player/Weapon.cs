﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform firePoint;
    //public GameObject bulletPrefab;
    public GameObject laserBullet;
    //public LineRenderer lineRenderer;
    public int damage = 10;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();

            //Not implied
            #region Shoot with Laserline
            //StartCoroutine(Shoot());
            #endregion
        }
    }

    void Shoot()
    {
        Instantiate(laserBullet, firePoint.position, firePoint.rotation);
    }
    //Not implied
    #region LaserLine //Not good//
    //IEnumerator Shoot()
    //{
    //    RaycastHit2D hitInfo = Physics2D.Raycast(firePoint.position, firePoint.right);
    //    if (hitInfo)
    //    {
    //        Debug.Log(hitInfo.transform.name);
    //        Enemy enemy = hitInfo.transform.GetComponent<Enemy>();
    //        if (enemy != null)
    //        {
    //            enemy.TakeDamage(damage);
    //        }
    //        Instantiate(impactEffect, hitInfo.point, Quaternion.identity);

    //        lineRenderer.SetPosition(0, firePoint.position);
    //        lineRenderer.SetPosition(1, hitInfo.point);

    //    }
    //    else
    //    {
    //        lineRenderer.SetPosition(0, firePoint.position);
    //        lineRenderer.SetPosition(1, firePoint.position + firePoint.right);
    //    }

    //    lineRenderer.enabled = true;

    //    yield return new WaitForSeconds(0.01f);

    //    lineRenderer.enabled = false;

    //}
    #endregion
}
