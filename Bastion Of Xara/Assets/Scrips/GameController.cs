﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public bool gamePlaying { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        gamePlaying = false;
        BeginGame();
    }

    private void BeginGame()
    {
        gamePlaying = true;
        TimerController.instance.BeginTimer();
    }
}
