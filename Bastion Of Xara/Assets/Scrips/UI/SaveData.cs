﻿using UnityEngine;
using System.Collections;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;
using System.IO;

public class SaveData 
{
    private static readonly HttpClient client = new HttpClient();

    public static void Save(Save save)
    {
        var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:3000/api/savegame");
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";

        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
            Debug.Log(JsonUtility.ToJson(save));
            streamWriter.Write(JsonUtility.ToJson(save));
        }

        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
            var result = streamReader.ReadToEnd();
            Debug.Log(result.ToString());
        }
    }

    public static Save Load(int saveNumber, int userId)
    {
        var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:3000/api/savegame/load");
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";
        Save save = new Save();
        save.SaveNumber = saveNumber;
        save.userId = userId;

        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
            Debug.Log(JsonUtility.ToJson(save));
            streamWriter.Write(JsonUtility.ToJson(save));
        }

        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
            var result = streamReader.ReadToEnd();
            Debug.Log(result.ToString());
            return  JsonUtility.FromJson<Save>(streamReader.ReadToEnd());
            
        }
        
    }
}
