﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileCollisionScript : MonoBehaviour
{
    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Bullet"))
        {
            if (col.gameObject != null)
            {
                Destroy(col.gameObject);
            }
        }
        if (col.gameObject.name.Equals("Spike"))
        {
            Destroy(col.gameObject,2f);
        }
    }

    public void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Bullet"))
        {
            DestroyImmediate(col.gameObject);
        }
    }

    public void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Bullet"))
        {
            DestroyImmediate(col.gameObject);
        }
    }
}
