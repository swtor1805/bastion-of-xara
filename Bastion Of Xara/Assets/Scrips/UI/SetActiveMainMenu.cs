﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveMainMenu : MonoBehaviour
{
    public GameObject Mainmenu;
    public GameObject LoginMenu;

    public void activateMenu()
    {
        LoginMenu.SetActive(true);
        Mainmenu.SetActive(true);
    }
}
