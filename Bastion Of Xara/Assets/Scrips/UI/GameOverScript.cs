﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    public Text Title;
    public GameObject TitelObjectHeader;
    GameObject health;
    public AudioSource Music;
    public AudioSource BulletSound;


    public void GameOver()
    {
        health = GameObject.Find("HealthBar");
        health.SetActive(false);
        Cursor.visible = true;
        TitelObjectHeader.SetActive(true);
        Time.timeScale = 0f;

    }

    public void Restart()
    {
        Time.timeScale = 1f;
        FindObjectOfType<LevelLoader>().RestartActuallyLevel();
    }

}
