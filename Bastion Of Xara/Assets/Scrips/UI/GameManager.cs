﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false;
    public int activeScene;
    public void GameOver()
    {
        if(gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("GameOver");
            activeScene = Convert.ToInt32(SceneManager.GetActiveScene().name);
            Invoke("GameOverScene",0.1f);

        }
    }

    public void GameOverScene()
    {
        SceneManager.LoadScene("GameOver");
    }


    public void Restart()
    {
        Debug.Log(activeScene);
        SceneManager.LoadScene(activeScene);
    }
}
