﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class LoginFromUnityUI : MonoBehaviour
{
    public GameObject username;
    public GameObject password;
    private string Username;
    private string Password;

    public void LoginUserByName()
    {
        User user = new User();
        user.password = Password;
        user.username = Username;
        Login.LoginUser(user);
    }

    public void DebugLogin()
    {
        SceneManager.LoadScene(1);
    }



    // Update is called once per frame
    void Update()
    {
        Username = username.GetComponent<InputField>().text;
        Password = password.GetComponent<InputField>().text;
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (username.GetComponent<InputField>().isFocused)
            {
                password.GetComponent<InputField>().Select();
            }
        }
    }
}
