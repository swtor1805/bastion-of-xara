﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class LevelComplet : MonoBehaviour
{
    public GameObject LevelFinished;
    GameObject health;
    public Text Title;
    public Text YourTime;
    public Text BestTime;
    public AudioSource Music;
    public AudioSource BulletSound;
    private int Bronze = 45;
    private int Silver = 39;
    private int Gold = 35;
    public GameObject BronzeStar;
    public GameObject SilverStar;
    public GameObject GoldStar;


    public void LevelDone()
    {
        Music.volume = 0;
        BulletSound.volume = 0;
        Cursor.visible = true;
        health = GameObject.Find("HealthBar");
        LevelFinished.SetActive(true);
        BestTime.text = "Best Time: 00:00:33.00";
        YourTime.text = "Your Time: " + FindObjectOfType<TextTimer>().PlayerTime;
        SetStars();
        Title.text = "Level " + (FindObjectOfType<Player>().playerIsInLevel - 1)  + " Complete";
        Time.timeScale = 0f;
    }
    public void LoadNextLevel()
    {
        Time.timeScale = 1f;
        Music.volume = 0.25f;
        BulletSound.volume = 0.5f;
        FindObjectOfType<LevelLoader>().LoadNextLevel();

    }

    public void Restart()
    {
        Time.timeScale = 1f;
        FindObjectOfType<LevelLoader>().RestartActuallyLevel();

    }
    public void SetStars()
    {
        if (FindObjectOfType<TextTimer>().playedTime <= Gold)
        {
            Debug.Log(FindObjectOfType<TextTimer>().playedTime);
            BronzeStar.SetActive(true);
            SilverStar.SetActive(true);
            GoldStar.SetActive(true);
            health.SetActive(false);
        }
        else if (FindObjectOfType<TextTimer>().playedTime <= Silver)
        {
            Debug.Log(FindObjectOfType<TextTimer>().playedTime);
            BronzeStar.SetActive(true);
            SilverStar.SetActive(true);
            health.SetActive(false);
        }
        else if (FindObjectOfType<TextTimer>().playedTime <= Bronze)
        {
            Debug.Log(FindObjectOfType<TextTimer>().playedTime);
            BronzeStar.SetActive(true);
            health.SetActive(false);
        }

    }
}
