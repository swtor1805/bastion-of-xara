﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class LevelComplete: MonoBehaviour
{
    public GameObject LevelFinished;
    GameObject health;
    public Text Title;
    public Text YourTime;
    public Text BestTime;
    public AudioSource Music;
    public AudioSource BulletSound;
    private int Bronze = 45;
    private int Silver = 40;
    private int Gold = 35;
    private int playerPlayedTime;
    public GameObject BronzeStar;
    public GameObject SilverStar;
    public GameObject GoldStar;


    public void LevelDone()
    {
        Music.volume = 0;
        BulletSound.volume = 0;
        Cursor.visible = true;
        health = GameObject.Find("HealthBar");
        LevelFinished.SetActive(true);
        BestTime.text = "Best Time: 00:00:33.00";
        YourTime.text = "Your Time: " + FindObjectOfType<TextTimer>().PlayerTime;
        SetStars();
        Title.text = "Level " + (FindObjectOfType<Player>().playerIsInLevel - 1)  + " Complete";
        health.SetActive(false);
        Time.timeScale = 0f;
    }
    public void LoadNextLevel()
    {
        Time.timeScale = 1f;
        Music.volume = 0.25f;
        BulletSound.volume = 0.5f;
        FindObjectOfType<LevelLoader>().LoadNextLevel();
    }

    public void Restart()
    {
        Time.timeScale = 1f;
        FindObjectOfType<LevelLoader>().RestartActuallyLevel();
    }

    public void SetStars()
    {
        switch (FindObjectOfType<Player>().playerIsInLevel)
        {
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                Gold = 40;
                Silver = 45;
                Bronze = 50;
                BestTime.text = "Best Time: 00:00:37.00";
                break;

            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
                Gold = 45;
                Silver = 50;
                Bronze = 55;
                BestTime.text = "Best Time: 00:00:43.00";
                break;
        }

        if (FindObjectOfType<TextTimer>().playedTime <= Gold)
        {
            BronzeStar.SetActive(true);
            SilverStar.SetActive(true);
            GoldStar.SetActive(true);
        }
        else if (FindObjectOfType<TextTimer>().playedTime <= Silver)
        {
            BronzeStar.SetActive(true);
            SilverStar.SetActive(true);
        }
        else if (FindObjectOfType<TextTimer>().playedTime <= Bronze)
        {
            BronzeStar.SetActive(true);
        }
    }
}
