﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public static int userId;
    int activeScene;
    public void SetUserId()
    {
    }
    public void PlayGame()
    {
        FindObjectOfType<LevelLoader>().LoadNextLevel();
    }

    public void SetActivScene(int currentLevel)
    {
        activeScene = currentLevel;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(activeScene);
    }

    public void ExitGame()
    {
        Application.Quit();
        Debug.Log(EventSystem.current.currentSelectedGameObject.name);

    }

    public void GoToMainMenu()
    {
        Debug.Log("geht");
        Time.timeScale = 1f;
        FindObjectOfType<PauseMenu>().gameIsPaused = false;
        FindObjectOfType<LevelLoader>().GoToMainMenu();
    }

    public void GoBackToMainMenuFromFinish()
    {
        Time.timeScale = 1f;
        FindObjectOfType<LevelLoader>().GoToMainMenu();
    }


    public void SaveGameData()
    {
        if(EventSystem.current.currentSelectedGameObject.name == "1")
        {
            Debug.Log(1);
            Save save = new Save();
            save.PlayerLvl = activeScene;
            save.userId = userId;
            save.SaveNumber = 1;
            SaveData.Save(save);
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "2")
        {
            Debug.Log(2);
            Save save = new Save();
            save.PlayerLvl = activeScene;
            save.userId = userId;
            save.SaveNumber = 2;
            SaveData.Save(save);
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "3")
        {
            Debug.Log(3);
            Save save = new Save();
            save.PlayerLvl = activeScene;
            save.userId = userId;
            save.SaveNumber = 3;
            SaveData.Save(save);
        }
    }

    public void LoadGameData()
    {
        if (EventSystem.current.currentSelectedGameObject.name == "1")
        {
            Save save = SaveData.Load(1, userId);
            SceneManager.LoadScene((int)save.PlayerLvl);
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "2")
        {
            Save save = SaveData.Load(2, userId);
            SceneManager.LoadScene((int)save.PlayerLvl);
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "3")
        {
            Save save = SaveData.Load(3, userId);
            SceneManager.LoadScene((int)save.PlayerLvl);
        }

    }
}
