﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Save
{
    public int userId;
    public int PlayerLvl;
    public int Kills;
    public int SaveNumber;
}
