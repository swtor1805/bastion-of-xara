﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class User
{
    public int userId;
    public string username;
    public string password;
    public bool ownsGame;
}
